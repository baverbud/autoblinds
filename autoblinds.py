import argparse
import schedule
import time
import json
import sys

from powerview_controller import PowerViewController
from get_gmail_messages import get_alarm_emails, get_creds
from loguru import logger

config = None
automations = []
action = None
scenes_by_id = {}
scenes_by_name = {}
last_scene_id = None

controller = None


def last_run(elem):
    return (elem.last_run is None, elem.last_run)


def next_run(elem):
    return (elem.next_run is None, elem.next_run)


def resume_schedule():
    # We want to load up the previous state that a blind was set to from the automation,
    # which might not be the most recent scene.
    position_to_shade = {}
    for room in controller.state["rooms"]:
        room_name = room["name"]
        for shade in room["shades"]:
            shade_id = shade["id"]
            shade_name = shade["ptName"]

            try:
                position = controller.get_most_recent_automation_position(
                    shade_id=shade_id
                )
            except Exception:
                logger.warning(
                    f"No recent automation found for {room_name}/{shade_name}"
                )
                continue

            if "override" in config["states"][action]:
                for override in config["states"][action]["override"]:
                    if (
                        room_name == override["room"]
                        and shade_name == override["shade"]
                    ):
                        position = (position[0], override["position"], "override")
            elif "skip_resume" in config:
                # See if this shade is listed in skip_resume.
                if any(
                    [
                        c["room"] == room_name and c["shade"] == shade_name
                        for c in config["skip_resume"]
                    ]
                ):
                    logger.info(f"Skipping {room_name}/{shade_name}")
                    continue

            logger.info(
                f"Setting {room_name}/{shade_name} to {position[1]} (scene {position[2]})"
            )

            if position[1] in position_to_shade.keys():
                position_to_shade[position[1]].append(shade_id)
            else:
                position_to_shade[position[1]] = [shade_id]

    for position, shade_ids in position_to_shade.items():
        controller.activate_shades(shade_ids, position)


def update_alarm(config):
    global action
    global scenes_by_name
    global scenes_by_id
    global last_scene_id
    global controller

    try:
        creds = get_creds()
        content = get_alarm_emails(creds, config["states"])
    except Exception as ex:
        logger.error(ex)
        return

    latest_action = None
    latest_datetime = None
    for message in content:
        try:
            if latest_datetime is None or latest_datetime < message["date"]:
                latest_datetime = message["date"]
                latest_action = message["action"]
        except KeyError as ex:
            logger.error(ex)
            logger.error(message)

    if latest_action != action:
        # Change in alarm state.
        action = latest_action

        logger.info(f"State changed to {action}")
        resume_schedule()


def activate(id):
    global controller
    global scenes_by_id
    global last_scene_id
    global action
    global config

    num_overrides = (
        len(config["states"][action]["override"])
        if "override" in config["states"][action]
        else 0
    )

    logger.info(
        f"Activating {scenes_by_id[id]['ptName']} with {num_overrides} override"
    )
    controller.activate_scene(
        scenes_by_id[id]["ptName"],
        config["states"][action]["override"]
        if "override" in config["states"][action]
        else [],
    )
    last_scene_id = id


def update_scheduler(config):
    global scenes_by_id
    global scenes_by_name
    global automations
    global controller
    schedule.clear()

    # Self-update every 5 minutes.
    schedule.every(5).minutes.at(":30").do(update_scheduler, config=config)

    # # Retry scene activation every minute
    # schedule.every(1).minutes.do(retry_scene, host=config["host"])

    # Poll for changes in emails every minute
    schedule.every(1).minutes.do(update_alarm, config=config)

    # Get the schedule and scenes
    try:
        controller.update()
    except Exception as e:
        logger.error(f"Failed to update controller: {e}")

    try:
        automations = controller.get_schedule()
    except Exception as e:
        logger.error(f"Failed to update automations: {e}")

    try:
        raw_scenes = controller.get_scenes()

        scenes_by_id = {}
        scenes_by_name = {}

        # Create the maps
        for s in raw_scenes:
            scenes_by_id[s["id"]] = s
            scenes_by_name[s["ptName"]] = scenes_by_id
    except Exception as e:
        logger.error(f"Failed to update scenes: {e}")

    count = 0

    # Go through the schedule and add them to the scheduler
    for automation in automations:
        # Resolve sunset/sunrise times
        automation_time = controller.resolve_time(automation)
        time_str = f"{automation_time['hour']:02d}:{automation_time['min']:02d}"

        if automation["days"] == 127:
            # Every day
            schedule.every().day.at(time_str).do(
                activate, id=automation["sceneId"]
            ).tag("automation")
        else:
            # Find out what days apply
            if automation["days"] & hex("0x01"):
                schedule.every().monday.at(time_str).do(
                    activate, id=automation["sceneId"]
                ).tag("automation")
            if automation["days"] & hex("0x02"):
                schedule.every().tuesday.at(time_str).do(
                    activate, id=automation["sceneId"]
                ).tag("automation")
            if automation["days"] & hex("0x04"):
                schedule.every().wednesday.at(time_str).do(
                    activate, id=automation["sceneId"]
                ).tag("automation")
            if automation["days"] & hex("0x08"):
                schedule.every().thursday.at(time_str).do(
                    activate, id=automation["sceneId"]
                ).tag("automation")
            if automation["days"] & hex("0x10"):
                schedule.every().friday.at(time_str).do(
                    activate, id=automation["sceneId"]
                ).tag("automation")
            if automation["days"] & hex("0x20"):
                schedule.every().saturday.at(time_str).do(
                    activate, id=automation["sceneId"]
                ).tag("automation")
            if automation["days"] & hex("0x40"):
                schedule.every().sunday.at(time_str).do(
                    activate, id=automation["sceneId"]
                ).tag("automation")
        count = count + 1


def main():
    global config
    global controller
    parser = argparse.ArgumentParser(
        prog="autoblinds",
        description="Run the powerview schedule and activate an away scene when alarm is armed.",
    )
    parser.add_argument("config_file")

    args = parser.parse_args()

    logger.remove()
    logger.add(sys.stderr, level="INFO")

    logger.info(f"Loading from {args.config_file}")
    with open(args.config_file) as f:
        config = json.loads(f.read())

    controller = PowerViewController(config["host"])

    update_scheduler(config)
    update_alarm(config)

    while True:
        schedule.run_pending()
        time.sleep(1)


if __name__ == "__main__":
    main()
