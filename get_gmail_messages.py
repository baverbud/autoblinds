from __future__ import print_function

from datetime import datetime

from loguru import logger

import os.path

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# If modifying these scopes, delete the file token.json.
SCOPES = ["https://www.googleapis.com/auth/gmail.readonly"]


# From https://www.thepythoncode.com/article/use-gmail-api-in-python#Searching_for_Emails
def search_messages(service, query):
    result = service.users().messages().list(userId="me", q=query).execute()
    messages = []
    if "messages" in result:
        messages.extend(result["messages"])

    # We only need the most recent message, so if there's multiple pages, ignore
    # while 'nextPageToken' in result:
    #     page_token = result['nextPageToken']
    #     result = service.users().messages().list(userId='me',
    #               q=query, pageToken=page_token).execute()
    #     if 'messages' in result:
    #         messages.extend(result['messages'])
    return messages


def get_alarm_emails(creds, states):
    try:
        # Call the Gmail API
        service = build("gmail", "v1", credentials=creds)

        messages = []

        for name, state in states.items():
            state_messages = search_messages(service, state["query"])
            for msg_idx in range(len(state_messages)):
                state_messages[msg_idx]["action"] = name

            messages.extend(state_messages)

        # We have a set of message ids and thread ids, need to get subject and time of email.
        for msg_idx in range(len(messages)):
            gmsg = (
                service.users()
                .messages()
                .get(userId="me", id=messages[msg_idx]["id"], format="full")
                .execute()
            )

            payload = gmsg["payload"]
            headers = payload.get("headers")

            if not headers:
                continue

            for header in headers:
                if header.get("name") == "Subject":
                    messages[msg_idx]["subject"] = header.get("value")
                elif header.get("name") == "Date":
                    messages[msg_idx]["date"] = datetime.strptime(
                        header.get("value"), "%a, %d %b %Y %H:%M:%S %z"
                    )

    except HttpError as error:
        # TODO(developer) - Handle errors from gmail API.
        print(f"An error occurred: {error}")
    return messages


def get_creds():
    """Get gmail credentials.
    Based on https://developers.google.com/people/quickstart/python.
    """
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists("token.json"):
        creds = Credentials.from_authorized_user_file("token.json", SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        try:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                raise Exception()
        except Exception:
            logger.warning("Refreshing credentials failed, requesting a new token")
            flow = InstalledAppFlow.from_client_secrets_file("credentials.json", SCOPES)
            creds = flow.run_local_server(port=0)

        # Save the credentials for the next run
        with open("token.json", "w") as token:
            token.write(creds.to_json())

    return creds


def main():
    creds = get_creds()
    content = get_alarm_emails(creds)

    print(content)


if __name__ == "__main__":
    main()
