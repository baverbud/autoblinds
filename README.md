# Overview

This project provides a way to run the automations for Hunter Douglas PowerView® shades and interrupt them when you go away (eg. via arming a house alarm).

I love using the schedule feature of PowerView to automate my blinds. However, there's no existing automation with my alarm company or IFTTT that would allow me to disable the schedule and set to an "away" mode.

This is mostly because the Gateway has an unsecured API that is only accessible over the local network. Any way of setting the blinds to away and resuming the schedule based on my alarm status was going to need an internal source.

After digging into the PowerView API, there's no documented way to enable/disable an automation either. To address this, autoblinds reads the schedule from the PowerView Gen3 Gateway and runs the schedule. Automations should still be created using the PowerView app, but they should be disabled.

All automations are run by this app. If you don't want one to run, you need to delete it from the PowerView app. 

Tasks remaining:
 * Determine last active scene for each shade
 * Resume schedule for each shade
 * Poll alarm status and check internal state is consistent
 * Event alarm state change

Tasks done:
 * Read automations and scenes from PowerView Gen3 Gateway
 * Execute schedule

# Usage

Create a virtual environment and install the python requirements:

```
python3 -m venv venv
pip install -r requirements.txt
```

If you are going to contribute to the project, install the pre-commit hooks for formatting:

```
pre-commit install
```

There are three utilities: activate_scene.py, get_scenes.py and get_schedule.py that are pretty self-explanatory and are used for debug and test purposes.

You can start autoblinds by running the python script:

```
python autoblinds.py http://192.168.1.234:80 Away
```

Replacing 192.168.1.234:80 with the endpoint for your PowerView Gen3 Gateway, and "Away" with the scene you want to activate when the alarm is activated.

# PowerView Gen3 Gateway Documentation

Enable swagger on your local gateway by visiting http://powerview-g3.local/gateway/swagger?enable=true

Once it has started you can visit http://powerview-g3.local:3002/ to view the API documentation. The endpoint to use with the python scripts will be listed in the "Servers" drop-down box.