import json
from loguru import logger
import requests
import geocoder
from datetime import datetime, timedelta
from astral import LocationInfo
from astral.sun import sun


class PowerViewController:
    host = None
    state = {}
    shades = {}

    active_scene = None
    active_override = None

    # Set default sunrise and sunset in case astral cannot connect to a provider.
    astral_times = {
        "sunrise": datetime.fromisoformat("2023-07-29T06:00:00.434809"),
        "sunset": datetime.fromisoformat("2023-07-29T08:00:00.434809"),
    }
    astral_time_day_number = None

    def __init__(self, host):
        self.host = host

        self.update()

    def update(self):
        # Querying /home reveals a lot more information - like shade positions in each scene -
        # than querying the sub endpoints.
        request_path = f"{self.host}/home"

        response = requests.get(request_path)
        if response.status_code != 200:
            raise Exception(
                f"{request_path} returned non-success status code {response.status_code}"
            )

        self.state = json.loads(response.text)

        self.shades = {}
        for room in self.state["rooms"]:
            for s in room["shades"]:
                self.shades[s["id"]] = s

    def resolve_shade(self, shade_id):
        # Find the shade name and room name
        for room in self.state["rooms"]:
            for shade in room["shades"]:
                if shade["id"] == shade_id:
                    return room["name"], shade["ptName"]

        raise Exception(f"Shade id {shade_id} not found")

    def get_shade_id(self, room_name, shade_name):
        for room in self.state["rooms"]:
            if room["name"] != room_name:
                continue

            for shade in room["shades"]:
                if shade["ptName"] != shade_name:
                    continue

                return shade["id"]

        raise Exception(f"Cannot find shade {shade_name} in {room_name}")

    def get_override_position(self, shade_id, override):
        room, shade_name = self.resolve_shade(shade_id)

        override_position = None
        for o in override:
            if o["room"] == room and o["shade"] == shade_name:
                override_position = o["position"]
                break

        return override_position

    def get_most_recent_automation_position(
        self, shade_id=None, shade_name=None, room_name=None
    ):
        if shade_id is None:
            shade_id = self.get_shade_id(room_name, shade_name)
        else:
            room_name, shade_name = self.resolve_shade(shade_id)

        # Position is going to be an array of "time since" and position.
        # We'll sort it by "time since" and then the first entry will be the position we want.
        positions = []

        now = datetime.now()

        # Find the most recent scene that set this shade and return the position.
        for automation in self.state["automations"]:
            scene_id = automation["scene_Id"]

            automation_time_dict = self.resolve_time(automation)
            automation_datetime = now
            automation_datetime = automation_datetime.replace(
                hour=automation_time_dict["hour"],
                minute=automation_time_dict["min"],
                second=0,
            )

            if automation_datetime > now:
                # Occured yesterday
                automation_datetime = now - timedelta(days=1)

            for scene in self.state["scenes"]:
                if scene_id != scene["_id"]:
                    continue

                # Check if this shade is present
                for shade in scene["members"]:
                    if shade["shd_Id"] != shade_id:
                        continue

                    positions.append(
                        (
                            (now - automation_datetime).total_seconds(),
                            shade["pos"]["pos1"] / 10000.0,
                            scene["name"],
                        )
                    )

        if len(positions) == 0:
            raise Exception(f"No recent automation found for shade {shade_id}")

        last_scene = sorted(positions)[0]
        logger.debug(
            f"Activating scene {last_scene[2]} for shade {room_name}/{shade_name}"
        )

        return last_scene

    def activate_scene(self, scene_name, overrides):
        # Instead of using the activate scene endpoint, we request each shade to move to the scene
        # location, and exclude any in the provided exclude list.
        scene_to_activate = None
        for scene in self.state["scenes"]:
            if scene_name == scene["name"]:
                scene_to_activate = scene
                break

        if not scene_to_activate:
            raise Exception(f"Could not find scene {scene_name}")

        # Activate using the scene API if no overrides are present.
        if len(overrides) == 0:
            # TODO: really we should do this if a scene is being activate and no overrides
            # apply to the scene, then still evaluate the remaining overrides.
            id = scene_to_activate["_id"]
            logger.info(f"Activating scene {scene_name}")
            scene_activation_path = f"{self.host}/home/scenes/{id}/activate"
            response = requests.put(scene_activation_path)
            return response.status_code == 200

        shades_activated = {}

        for shade in scene_to_activate["members"]:
            shade_id = shade["shd_Id"]

            override_position = self.get_override_position(shade_id, overrides)
            shade_position = (
                override_position
                if override_position is not None
                else (shade["pos"]["pos1"] / 10000)
            )

            shades_activated[shade_id] = shade_position

        # Process remaining overrides, since overrides apply independent of scene and
        # may not be in this scene.
        for o in overrides:
            try:
                shade_id = self.get_shade_id(o["room"], o["shade"])
                if shade_id in shades_activated:
                    continue
            except Exception as ex:
                logger.error(ex)
                continue

            shades_activated[shade_id] = o["position"]

        try:
            self.activate_shades(
                shades_activated.keys(),
                [shades_activated[id] for id in shades_activated.keys()],
            )
        except Exception as ex:
            logger.error(ex)

    def activate_shades(self, ids, position):
        path = (
            f"{self.host}/home/shades/positions?ids={','.join([str(id) for id in ids])}"
        )
        payload = {"positions": {"primary": position}}

        try:
            response = requests.put(path, json=payload)
            if response.status_code != 200:
                raise Exception(response)

            # Even though the resonse status is 200, the shade might still have failed. Check the
            # status code in the response.
            data = json.loads(response.text)

            if data["err"] != 0:
                # One or more shades failed.
                for s in data["responses"]:
                    if s["err"] != 0:
                        shade_name = self.shades[s["id"]]["ptName"]
                        logger.error(
                            f"Shade {shade_name} failed to respond: {s['dbgMsg']}"
                        )

            return True
        except Exception as ex:
            logger.error(ex)

        return False

    def get_scenes(self):
        scene_activation_path = f"{self.host}/home/scenes"

        response = requests.get(scene_activation_path)

        if response.status_code != 200:
            raise Exception(
                f"{scene_activation_path} returned non-success status code {response.status_code}"
            )

        return json.loads(response.text)

    def get_schedule(self):
        scene_activation_path = f"{self.host}/home/automations"

        response = requests.get(scene_activation_path)

        if response.status_code != 200:
            raise Exception(f"Failed to get schedule, code {response.status_code}")

        j = json.loads(response.text)

        return j

    def get_astral_times(self):
        now_date = datetime.now().date()
        if self.astral_times is None or self.astral_time_day_number != now_date.day:
            try:
                self.astral_times = self.fetch_astral_times()
                self.astral_time_day_number = now_date.day
            except Exception:
                logger.error(
                    "Failed to update sunrise times, "
                    + "using previous times and will attempt again next update"
                )
        return self.astral_times

    def fetch_astral_times(self):
        g = geocoder.ip("me")
        loc = LocationInfo(latitude=g.latlng[0], longitude=g.latlng[1])
        s = sun(loc.observer, date=datetime.now(), tzinfo=datetime.now().tzinfo)
        return s

    def resolve_time(self, automation):
        astral_t = self.get_astral_times()

        if automation["type"] == 0:
            # Clock based
            return {"hour": automation["hour"], "min": automation["min"]}
        elif automation["type"] == 2:
            # Before sunrise
            start_time = astral_t["sunrise"] - timedelta(
                hours=automation["hour"], minutes=automation["min"]
            )
            return {
                "hour": start_time.time().hour,
                "min": start_time.time().minute,
            }
        elif automation["type"] == 6:
            # Before sunset
            start_time = astral_t["sunset"] - timedelta(
                hours=automation["hour"], minutes=automation["min"]
            )
            return {
                "hour": start_time.time().hour,
                "min": start_time.time().minute,
            }
        elif automation["type"] == 10:
            # After sunrise
            start_time = astral_t["sunrise"] + timedelta(
                hours=automation["hour"], minutes=automation["min"]
            )
            return {
                "hour": start_time.time().hour,
                "min": start_time.time().minute,
            }
        elif automation["type"] == 14:
            # After sunset
            start_time = astral_t["sunset"] + timedelta(
                hours=automation["hour"], minutes=automation["min"]
            )
            return {
                "hour": start_time.time().hour,
                "min": start_time.time().minute,
            }

        raise Exception(f"Unknown automation type {automation['type']}")
